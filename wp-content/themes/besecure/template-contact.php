<?php
/*
Template name: Contact
*/
?>

<?php get_header(); ?>

	<div class="content">

		<div class="twothird_column">

			<?php
			if( have_posts() ) :
				while( have_posts() ) :
					the_post();
					
					get_template_part('content', 'page');

				endwhile;
			endif;
			?>

		</div>

		<div class="onethird_column">

			<h2>Contactpersonen</h2>

			<ul class="contacts">
				<li>
					<p class="name">Pieter Van Overwalle</p>
					<p class="function">Zaakvoerder</p>
					<p class="contact-details"><a href="mailto:info@be-secure.be">pieter@be-secure.be</a><br />
					<strong>Tel: </strong>+32 471 28 62 24</p>
				</li>
				<li>
					<p class="name">Tom Van den Bossche</p>
					<p class="function">Project engineer</p>
					<p class="contact-details"><a href="mailto:tom@be-secure.be">tom@be-secure.be</a><br />
					<strong>Tel: </strong>+32 472 02 50 20</p>
				</li>
			</ul>

		</div>

	</div>

<?php get_footer(); ?>