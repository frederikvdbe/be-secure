	<div class="bottom">
			
		<ul class="parent-list">
			<li class="parent_list"><h4>Oplossingen</h4>
				<ul>
					<li><a href="<?php echo get_permalink( 5 ); ?>">Camerabewaking</a></li>
					<li><a href="<?php echo get_permalink( 248 ); ?>">Netwerkbeheer</a></li>
					<li><a href="<?php echo get_permalink( 179 ); ?>">Videoparlofonie</a></li>
				</ul>
			</li>
		
			<li class="parent_list"><h4>Extra diensten</h4>
				<ul>
					<li><a href="<?php echo get_permalink( 175 ); ?>">Toegangscontrole</a></li>
					<li><a href="<?php echo get_permalink( 246 ); ?>">Automatisering</a></li>
				</ul>
			</li>

			<li class="parent_list"><h4>Partners</h4>
				<ul>
					<li><a href="http://www.milestonesys.com/" target="_blank">Milestone</a></li>
					<li><a href="http://www.mobotix.com/" target="_blank">Mobotix</a></li>
					<li><a href="http://www.vivotek.com/" target="_blank">Vivotek</a></li>
					<li><a href="http://www.dell.com/" target="_blank">Dell Computers</a></li>
				</ul>
			</li>

			<li class="parent_list"><h4>Bedrijfsgegevens</h4>
				<ul>
					<li>BE0662.866.920</li>
					<li>+32 471 28 62 24</li>
					<li><a href="mailto:info@be-secure.be">info@be-secure.be</a></li>
					<li><a href="https://www.facebook.com/besecurecamerabewaking" target="_blank">Bezoek onze Facebook pagina</a></li>
				</ul>
			</li>
		</ul>
		
	</div>
	
	<div class="footer">

		<div class="footer--left">
			Alle rechten voorbehouden aan be-secure.be
		</div>
			
		<div class="footer--right">
			
			<?php

			$defaults = array(
				'theme_location'  => 'footer-nav',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'footernav',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $defaults );

			?>

		</div>

	</div>

</div><!-- // WRAP -->

<div class="circles"></div>

<?php /*
<div id="modal_invoice" class="reveal-modal">
	<h3>Offerte aanvraag</h3>
	<p>Vul hieronder alle gevraagde velden in en wij nemen zo snel mogelijk contact met u op.</p>
    <?php echo do_shortcode('[contact-form-7 id="184" title="Offerte"]'); ?>
</div>

<div id="starter" class="reveal-modal">
	<h3>Starter Pakket</h3>
    <p style="margin:20px 0;">Bent u een kleine zelfstandige en wenst u uw kantoor of werkplaats beter te beveiligen wanneer u niet fysiek aanwezig bent? Dan biedt ons starter pakket de ideale basis. Met 1 buitencamera en 3 binnencamera's kan u met een gerust hart de deur achter u sluiten.</p>
	<p style="margin:20px 0;">Onze prijzen zijn inclusief hardware, installatie en configuratie. Geen onvoorziene kosten.</p>	
	<div class="ctas">
		<span class="btn btn-grey">&euro;2699 excl. BTW</span>
		<a href="<?php echo get_page_link(13); ?>" class="btn">Contacteer ons</a>
	</div>
</div>

<div id="advanced" class="reveal-modal">
	<h3>Premium Pakket</h3>
    <p style="margin:20px 0;">Het premium pakket is uitermate geschikt om de verschillende ruimtes in uw kantoorgebouw te beveiligen. Bovendien beschikt u een buitencamera met breedhoeklens waarmee geen enkele verdachte beweging u ontgaat.</p>
	<p style="margin:20px 0;">Onze prijzen zijn inclusief hardware, installatie en configuratie. Geen onvoorziene kosten.</p>
	<div class="ctas">
		<span class="btn btn-grey">&euro;3899 excl. BTW</span>
		<a href="<?php echo get_page_link(13); ?>" class="btn">Contacteer ons</a>
	</div>
</div>

<div id="professional" class="reveal-modal">
	<h3>Advanced Pakket</h3>
    <p style="margin:20px 0;">Uw KMO heeft ongetwijfeld waardevolle apparaten of machines staan. Bovendien werkt u met een aantal medewerkers die toegang hebben tot verschillende delen van uw bedrijf en vereist de omvang van uw terrein dat er verschillende buitencamera's geplaatst worden. Met dit pakket verliest u niets uit het oog.</p>
	<p style="margin:20px 0;">Onze prijzen zijn inclusief hardware, installatie en configuratie. Geen onvoorziene kosten.</p>
	<div class="ctas">
		<span class="btn btn-grey">&euro;5499 excl. BTW</span>
		<a href="<?php echo get_page_link(13); ?>" class="btn">Contacteer ons</a>
	</div>
</div>
*/ ?>

<!-- LiveZilla Tracking Code (ALWAYS PLACE IN BODY ELEMENT) --><div id="livezilla_tracking" style="display:none"></div><script type="text/javascript">
var script = document.createElement("script");script.async=true;script.type="text/javascript";var src = "http://be-secure.be/livezilla/server.php?a=fd5c2&request=track&output=jcrpt&ovlp=MjI_&ovlc=IzAwODBmZg__&ovlct=I2ZmZmZmZg__&ovlt=T25saW5lIC0gQ2hhdCBtZXQgb25z&ovlto=T2ZmbGluZSAtIExhYXQgZWVuIGJlcmljaHQgYWNodGVy&eca=MQ__&ecw=Mjgw&ech=OTA_&ecmb=Mjc_&echt=UXVlc3Rpb25zPw__&echst=Q2hhdCB3aXRoIHVzIGxpdmU_&ecoht=UXVlc3Rpb25zPw__&ecohst=UGxlYXNlIGxlYXZlIGEgbWVzc2FnZQ__&ecfs=I0YwRkZENQ__&ecfe=I0QzRjI5OQ__&echc=IzZFQTMwQw__&ecslw=Mg__&ecsgs=IzY1OUYyQQ__&ecsge=IzczQkUyOA__&ecsa=MQ__&ecsb=NA__&ecsx=Mw__&ecsy=Mw__&ecsc=IzQ2NDY0Ng__&nse="+Math.random();setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)",1);</script><noscript><img src="http://be-secure.be/livezilla/server.php?a=fd5c2&amp;request=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript><!-- http://www.LiveZilla.net Tracking Code -->

<?php wp_footer(); ?>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117763307-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117763307-2');
</script>

</div>
</body>
</html>