<?php
/*
Template name: Products
*/
?>

<?php get_header(); ?>

	<div class="content">

		<div class="onethird_column_left">

			<h2>Categoriën</h2>

			<?php

			$catTax = array( 
			    'product_categories'
			);

			$catArgs = array(
			    'orderby'           => 'name', 
			    'order'             => 'ASC',
			    'hide_empty'        => false, 
			    'exclude'           => array(), 
			    'exclude_tree'      => array(), 
			    'include'           => array(),
			    'number'            => '', 
			    'fields'            => 'all', 
			    'slug'              => '', 
			    'parent'            => '',
			    'hierarchical'      => true, 
			    'child_of'          => 0, 
			    'get'               => '', 
			    'name__like'        => '',
			    'description__like' => '',
			    'pad_counts'        => false, 
			    'offset'            => '', 
			    'search'            => '', 
			    'cache_domain'      => 'core'
			); 

			$cats = get_terms( $catTax, $catArgs );

			if( $cats ){
				?><ul class="side list list_cats"><?php
				foreach( $cats as $cat ){
					$cat_link = get_term_link( $cat );
					echo '<li><a href="' . $cat_link . '">' . $cat->name . '</a></li>';
				}
				?></ul><?php
			}

			?>
		</div>

		<div class="twothird_column_right">

			<?php

			$prodArgs = array(
				'post_type' => 'product'
			);

			$products = new WP_Query($prodArgs);

			if( $products->have_posts() ) :
				?><ul class="product_list"><?php
				while( $products->have_posts() ) :
					$products->the_post();
			
					get_template_part('content', 'product');

				endwhile;
				?></ul><?php
			endif;
			?>

		</div>

	</div>

<?php get_footer(); ?>