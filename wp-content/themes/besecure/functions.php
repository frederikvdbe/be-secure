<?php

/*-----------------------------------------------------------------------------------*/
/* Remove Header Links */
/*-----------------------------------------------------------------------------------*/
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rel_canonical' ); // Display the canonical link rel
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


/*-----------------------------------------------------------------------------------*/
/* Theme Settings */
/*-----------------------------------------------------------------------------------*/
// Post Thumbnail Init
add_theme_support( 'post-thumbnails' );


/*-----------------------------------------------------------------------------------*/
/* Project Enqueues */
/*-----------------------------------------------------------------------------------*/

// JAVASCRIPT
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' ); 
function custom_enqueue_scripts() {

  // Reveal modals
  wp_enqueue_script( 'reveal-js', get_stylesheet_directory_uri() . '/assets/js/jquery.reveal.js', '', '', true );

  // Owl Carrousel
  // if( is_page('home') ){
    wp_enqueue_script( 'owl-js', get_stylesheet_directory_uri() . '/assets/js/owl.carousel.min.js', '', '', true );
  // }

  // Custom JS
  wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', '', array('reveal-js', 'owl-js'), true );
    
}

// CSS
add_action( 'wp_enqueue_scripts', 'custom_enqueue_styles' ); 
function custom_enqueue_styles() {

    // Custom styles
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', '', '1.1', all );

    // Google Fonts
    wp_register_style('open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800');
    wp_enqueue_style( 'open-sans');
}


/*-----------------------------------------------------------------------------------*/
/* Menus init */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'register_menus' );

function register_menus() {
	register_nav_menus(
		array(
			'site-nav' => __( 'Site Navigation', 'custom_theme' ),
      'footer-nav' => __( 'Footer Navigation', 'custom_theme' )
		)
	);
}



/*-----------------------------------------------------------------------------------*/
/* Image Sizes */
/*-----------------------------------------------------------------------------------*/
//add_image_size( 'small', 160, true );
//update_option('thumbnail_size_w', 280);
//update_option('thumbnail_size_h', 210);
//update_option('medium_size_w', 480);
//update_option('medium_size_h', 360);
//update_option('large_size_w', 680);
//update_option('large_size_h', 510);



/*-----------------------------------------------------------------------------------*/
/* Disable Alerts In Dashboard */
/*-----------------------------------------------------------------------------------*/
if ( !current_user_can('activate_plugins') ) {

  // Remove WP Update nag
  add_action('admin_menu','wphidenagdefault');
  function wphidenagdefault() {
    remove_action( 'admin_notices', 'update_nag', 3 );
  }

  // Remove Plugin Update Nag
  remove_action( 'load-update-core.php', 'wp_update_plugins' );
  add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

}


/*-----------------------------------------------------------------------------------*/
/* Selectively Hide WP Admin Menus */
/*-----------------------------------------------------------------------------------*/
add_action('admin_init', 'ns_remove_admin_menus');
function ns_remove_admin_menus() {
  global $menu;
  global $current_user;
  if ($current_user->ID != 1) {
    remove_menu_page('plugins.php');
    remove_menu_page('options-general.php');
    remove_menu_page('tools.php');
    remove_menu_page('edit-comments.php');
    remove_menu_page('edit.php?post_type=acf');
    remove_submenu_page( 'themes.php', 'themes.php' );
    remove_submenu_page( 'themes.php', 'customize.php' );
    remove_submenu_page( 'themes.php', 'theme-editor.php' );
    remove_submenu_page( 'index.php', 'update-core.php' );
  }
}

if ($current_user->ID != 1) {

  // Remove WP Update nag
  add_action('admin_menu','wphidenag');
  function wphidenag() {
  remove_action( 'admin_notices', 'update_nag', 3 );
  }

  // Remove Plugin Update Nag
  remove_action( 'load-update-core.php', 'wp_update_plugins' );
  add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

}


/*-----------------------------------------------------------------------------------*/
/* Name Posts
/*-----------------------------------------------------------------------------------*/
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );


/*-----------------------------------------------------------------------------------*/
/* Fixing current page selectors */
/*-----------------------------------------------------------------------------------*/

// Helper function
function is_child($page_id) { 
    global $post; 
    if( is_page() && ($post->post_parent == $page_id) ) {
       return true;
    } else { 
       return false; 
    }
}

function remove_parent_classes($class)
{
  // check for current page classes, return false if they exist.
  return ($class == 'current_page_item' || $class == 'current_page_parent' || $class == 'current_page_ancestor'  || $class == 'current-menu-item') ? FALSE : TRUE;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 3);
function special_nav_class($classes, $item){

     if( is_tax('product_categories') ) { //you can change the conditional from is_single() and $item->title
        
      $classes = array_filter($classes, "remove_parent_classes");

        if( $item->object_id == 181 ){
          $classes[] = 'current-menu-item';
        }

     }

     return $classes;
}

function product_categories_archive ( $query ) {
    if( $query->is_main_query() && is_tax('product_categories') ){
    $query->set( 'post_type', 'product' );
      // $query->set( 'orderby', 'title' );
      // $query->set( 'order', 'ASC' );
  }
}
add_action( 'pre_get_posts', 'product_categories_archive' );



/*-----------------------------------------------------------------------------------*/
/* Google analytics integration */
/*-----------------------------------------------------------------------------------*/
add_action('wp_footer', 'add_googleanalytics');
function add_googleanalytics() { ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53945742-1', 'auto');
  ga('send', 'pageview');

</script>

<?php }


/*-----------------------------------------------------------------------------------*/
/* Includes */
/*-----------------------------------------------------------------------------------*/
require_once ( 'includes/cpts.php' );
//require_once ( 'includes/sidebars.php' );
//require_once ( 'includes/widgets.php' );
//require_once ( 'includes/shortcodes.php' );

?>
