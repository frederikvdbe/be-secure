<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php

/*-----------------------------------------------------------------------------------*/
/* Sidebars */
/*-----------------------------------------------------------------------------------*/

/*
add_action( 'init', 'widgets_init' );

function widgets_init() {
	
    if ( !function_exists( 'register_sidebar') )
    return;

	register_sidebar(
		array(
			'name' => __( 'Home', 'woothemes' ),
			'id' => 'home',
			'description' => __( 'Home sidebar for zucsu.', 'woothemes' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		)
	);

	register_sidebar(
		array(
			'name' => __( 'Blog', 'woothemes' ),
			'id' => 'blog',
			'description' => __( 'Blog sidebar for zucsu.', 'woothemes' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		)
	);
    
}
*/