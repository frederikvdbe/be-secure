<?php
// File Security Check
if ( ! empty( $_SERVER['SCRIPT_FILENAME'] ) && basename( __FILE__ ) == basename( $_SERVER['SCRIPT_FILENAME'] ) ) {
    die ( 'You do not have sufficient permissions to access this page' );
}
?>
<?php


/*-----------------------------------------------------------------------------------*/
/* CPT VACANCIES */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'custom_post_type_vacancies' );

function custom_post_type_vacancies() {
      
  $labels = array(
    'name' => __('Vacancies', 'customtheme'),
    'singular_name' => __('Vacancies', 'customtheme'),
    'add_new' => __('Add New', 'customtheme'), __('Vacancy', 'customtheme'),
    'add_new_item' => __('Vacancy', 'customtheme'),
    'edit_item' => __('Edit Vacancy', 'customtheme'),
    'new_item' => __('New Vacancy', 'customtheme'),
    'view_item' => __('View Vacancy', 'customtheme'),
    'search_items' => __('Search Vacancies', 'customtheme'),
    'not_found' =>  __('No Vacancies found', 'customtheme'),
    'not_found_in_trash' => __('No Vacancies found in Trash', 'customtheme'),
    'parent_item_colon' => ''
  );

  $supports = array(
    'title',
    'editor',
    'categories',
    'excerpt',
    'thumbnail'
  );
  
  register_post_type( 'vacancy',
    array(
      'labels' => $labels,
      'public' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-megaphone',
      'supports' => $supports
    )
  );
  
}


// Custom Taxonomy
// add_action( 'init', 'custom_taxonomy_vacancyCats', 0 );  

// function custom_taxonomy_vacancyCats() {
    
//   // Recipe Type Custom Taxonomy
//   $recipe_type_labels = array(
//       'name' => __('Categories', 'customtheme'),
//       'singular_name' => __('Category', 'customtheme'),
//       'search_items' => __('Search Categories', 'customtheme'),
//       'all_items' => __('All Categories', 'customtheme'),
//       'parent_item' => __('Parent Category', 'customtheme'),
//       'parent_item_colon' =>__('Parent Category:', 'customtheme'),
//       'edit_item' => __('Edit Category', 'customtheme'), 
//       'update_item' => __('Update Category', 'customtheme'),
//       'add_new_item' => __('Add Category', 'customtheme'),
//       'new_item_name' => __('Category Name', 'customtheme'),
//       'menu_name' => __('Categories', 'customtheme')
//     ); 


//   register_taxonomy(
//       'artist_categories',
//       array( 'artist' ),
//       array(
//         'hierarchical' => true,
//         'labels' => $recipe_type_labels,
//         'query_var' => true,
//       )
//   );
// }



/*-----------------------------------------------------------------------------------*/
/* CPT REFERENCES */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'custom_post_type_references' );

function custom_post_type_references() {
      
  $labels = array(
    'name' => __('References', 'customtheme'),
    'singular_name' => __('References', 'customtheme'),
    'add_new' => __('Add New', 'customtheme'), __('Reference', 'customtheme'),
    'add_new_item' => __('Reference', 'customtheme'),
    'edit_item' => __('Edit Reference', 'customtheme'),
    'new_item' => __('New Reference', 'customtheme'),
    'view_item' => __('View Reference', 'customtheme'),
    'search_items' => __('Search References', 'customtheme'),
    'not_found' =>  __('No References found', 'customtheme'),
    'not_found_in_trash' => __('No References found in Trash', 'customtheme'),
    'parent_item_colon' => ''
  );

  $supports = array(
    'title',
    'editor',
    'categories',
    'excerpt',
    'thumbnail'
  );
  
  register_post_type( 'reference',
    array(
      'labels' => $labels,
      'public' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-portfolio',
      'supports' => $supports
    )
  );
  
}


// Custom Taxonomy
// add_action( 'init', 'custom_taxonomy_vacancyCats', 0 );  

// function custom_taxonomy_vacancyCats() {
    
//   // Recipe Type Custom Taxonomy
//   $recipe_type_labels = array(
//       'name' => __('Categories', 'customtheme'),
//       'singular_name' => __('Category', 'customtheme'),
//       'search_items' => __('Search Categories', 'customtheme'),
//       'all_items' => __('All Categories', 'customtheme'),
//       'parent_item' => __('Parent Category', 'customtheme'),
//       'parent_item_colon' =>__('Parent Category:', 'customtheme'),
//       'edit_item' => __('Edit Category', 'customtheme'), 
//       'update_item' => __('Update Category', 'customtheme'),
//       'add_new_item' => __('Add Category', 'customtheme'),
//       'new_item_name' => __('Category Name', 'customtheme'),
//       'menu_name' => __('Categories', 'customtheme')
//     ); 


//   register_taxonomy(
//       'artist_categories',
//       array( 'artist' ),
//       array(
//         'hierarchical' => true,
//         'labels' => $recipe_type_labels,
//         'query_var' => true,
//       )
//   );
// }


/*-----------------------------------------------------------------------------------*/
/* CPT PRODUCTS */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'custom_post_type_products' );

function custom_post_type_products() {
      
  $labels = array(
    'name' => __('Products', 'customtheme'),
    'singular_name' => __('Products', 'customtheme'),
    'add_new' => __('Add New', 'customtheme'), __('Product', 'customtheme'),
    'add_new_item' => __('Product', 'customtheme'),
    'edit_item' => __('Edit Product', 'customtheme'),
    'new_item' => __('New Product', 'customtheme'),
    'view_item' => __('View Product', 'customtheme'),
    'search_items' => __('Search Products', 'customtheme'),
    'not_found' =>  __('No Products found', 'customtheme'),
    'not_found_in_trash' => __('No Products found in Trash', 'customtheme'),
    'parent_item_colon' => ''
  );

  $supports = array(
    'title',
    'editor',
    'categories',
    'excerpt',
    'thumbnail'
  );
  
  register_post_type( 'product',
    array(
      'labels' => $labels,
      'public' => true,
      'hierarchical' => false,
      'has_archive' => true,
      'menu_icon' => 'dashicons-archive',
      'supports' => $supports
    )
  );
  
}


// Custom Taxonomy
add_action( 'init', 'custom_taxonomy_productCats', 0 );  

function custom_taxonomy_productCats() {
    
  // Recipe Type Custom Taxonomy
  $recipe_type_labels = array(
      'name' => __('Categories', 'customtheme'),
      'singular_name' => __('Category', 'customtheme'),
      'search_items' => __('Search Categories', 'customtheme'),
      'all_items' => __('All Categories', 'customtheme'),
      'parent_item' => __('Parent Category', 'customtheme'),
      'parent_item_colon' =>__('Parent Category:', 'customtheme'),
      'edit_item' => __('Edit Category', 'customtheme'), 
      'update_item' => __('Update Category', 'customtheme'),
      'add_new_item' => __('Add Category', 'customtheme'),
      'new_item_name' => __('Category Name', 'customtheme'),
      'menu_name' => __('Categories', 'customtheme')
    ); 


  register_taxonomy(
      'product_categories',
      array( 'product' ),
      array(
        'hierarchical' => true,
        'labels' => $recipe_type_labels,
        'query_var' => true,
      )
  );
}


/*-----------------------------------------------------------------------------------*/
/* CPT FAQ */
/*-----------------------------------------------------------------------------------*/
add_action( 'init', 'custom_post_type_faq' );

function custom_post_type_faq() {
      
  $labels = array(
    'name' => __('FAQ', 'customtheme'),
    'singular_name' => __('FAQ', 'customtheme'),
    'add_new' => __('Add New', 'customtheme'), __('FAQ', 'customtheme'),
    'add_new_item' => __('FAQ', 'customtheme'),
    'edit_item' => __('Edit FAQ', 'customtheme'),
    'new_item' => __('New FAQ', 'customtheme'),
    'view_item' => __('View FAQ', 'customtheme'),
    'search_items' => __('Search FAQ', 'customtheme'),
    'not_found' =>  __('No FAQ found', 'customtheme'),
    'not_found_in_trash' => __('No FAQ found in Trash', 'customtheme'),
    'parent_item_colon' => ''
  );

  $supports = array(
    'title',
    'editor',
    'categories',
    'excerpt',
    'thumbnail'
  );
  
  register_post_type( 'faq',
    array(
      'labels' => $labels,
      'public' => true,
      'hierarchical' => false,
      'has_archive' => false,
      'menu_icon' => 'dashicons-clipboard',
      'supports' => $supports
    )
  );
  
}

?>