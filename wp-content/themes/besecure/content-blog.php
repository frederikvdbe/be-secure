<li>
	<?php
	if( the_field('page_content_title') ){

		?><h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_field('page_content_title'); ?></a></h2>

	<?php } else {
		
		?><h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2><?php

	}

	the_excerpt();

	?>
</li>