'use strict';

var bower_path = "./assets/bower";
 
var gulp = require('gulp');
var copy = require('gulp-copy');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
 
/* CSS */
gulp.task('sass', function () {
	gulp.src('./assets/scss/style.scss')
    	.pipe(sass().on('error', sass.logError))
    	.pipe(gulp.dest('./'));
});


gulp.task('sass:watch', function () {
	gulp.watch('./assets/scss/**/*.scss', ['sass']);
});


/* JS */
// gulp.task('scripts', function () {
// 	gulp.src([
//     	bower_path + '/fancybox/source/jquery.fancybox.js',
//     	'./assets/js/custom.js'
//     ])
//     .pipe(concat('concat.js'))
//     .pipe(uglify())
//     .pipe(rename('scripts.js'))
//     .pipe(gulp.dest('./assets/js/'));
// });

// gulp.task('scripts:watch', function () {
//   gulp.watch('./assets/js/custom.js', ['scripts']);
// });