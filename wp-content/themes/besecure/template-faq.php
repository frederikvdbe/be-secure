<?php
/*
Template name: FAQ
*/
?>
<?php get_header(); ?>

<?php if( has_post_thumbnail( ) ){
	echo '<div class="page-header">';
	the_post_thumbnail();
	echo '</div>';
} ?>

<div class="content">

<?php if( have_posts() ) :
	while( have_posts() ) :
		the_post();
	?>
		<?php get_template_part('content', 'page' ); ?>
	<?php
	endwhile;
endif;
?>

<?php
$prodArgs = array(
	'post_type' => 'faq'
);$faq = new WP_Query($prodArgs);if( $faq->have_posts() ) :
	?><ul class="faq_list"><?php
	while( $faq->have_posts() ) :
		$faq->the_post();

		get_template_part('content', 'faq');	endwhile;
	?></ul><?php
endif;
?>

</div>
	
<?php get_footer(); ?>