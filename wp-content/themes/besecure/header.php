<?php
?><!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="icon" href="<?php bloginfo( 'url' ); ?>/favicon.png">
<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="wrap">

	<div class="heading">

		<div class="heading--logo">
			<a href="<?php bloginfo( 'url' ); ?>"><img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/besecure-logo.png" alt="" /></a>
		</div>

		<div class="heading-cta">
				<a href="<?php echo get_permalink(396); ?>">Gratis offerte</a>
			</div>

		<div class="heading--nav">

			<?php

			$defaults = array(
				'theme_location'  => 'site-nav',
				'menu'            => '',
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'sitenav',
				'menu_id'         => '',
				'echo'            => true,
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);

			wp_nav_menu( $defaults );

			?>

			<div class="heading--nav--resp">
				<a href="" class="toggle_resp_nav">MENU</a>
			</div>

		</div>

	</div>

	<div class="resp_nav">
		<?php wp_nav_menu( $defaults ); ?>
	</div>