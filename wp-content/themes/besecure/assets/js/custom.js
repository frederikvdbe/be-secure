jQuery(function($){

    // Custom responsive nav
    $('.toggle_resp_nav').click( function(e){
        e.preventDefault();
        $('.resp_nav').slideToggle();
    })

    // Owl Carrousel Init
    $("#home_refs").owlCarousel({
    	autoPlay : 2000,
    });

});