<?php
/*
Template name: Homepage
*/
?>

<?php get_header(); ?>

	<div class="slider">
		<?php echo do_shortcode('[rev_slider main]'); ?>
	</div>

	<div class="content">

		<div class="twothird_column">

			<?php
			if( have_posts() ) :
				while( have_posts() ) :
					the_post();
			
					get_template_part('content', 'page');

				endwhile;
			endif;
			?>

		</div>

		<div class="onethird_column">

			<?php

			$args = array(
				'post_type' => 'page',
				'posts_per_page' => 5,
				'post_parent' => 9,
				'order_by' => 'date',
				'order' => 'DESC'
			);

			$services = new WP_Query($args);

			if( $services->have_posts() ) :
				?><ul class="home-news"><h2>Onze diensten</h2><?php
				while ($services->have_posts() ) :
					$services->the_post() ;
					?>
					
					<li><a href="<?php the_permalink(); ?>">
						<?php the_title(); ?>
					</a></li>

					<?php

				endwhile;
				?></ul><?php
			endif;

			?>
		</div>

	</div>

	<div class="big-cta big-cta-full hide-on-desktop">
		<a href="#" data-reveal-id="modal_invoice">Klik hier voor een gratis offerte</a>
	</div>

	<?php /*
	<div class="full-width clearfix home-packets">

		<div class="block-intro">
			<h2>Onze drie populairste oplossingen</h2>
			<p>Onderstaande pakketten zijn voorzien van de beste kwaliteitsproducten en van de meest recente software van <a href="http://www.milestonesys.com/" target="_blank">Milestone</a>, de markleider op vlak van gebruiksvriendelijke beveiligingssoftware. Al onze pakketten zijn aan te passen op maat om aan uw noden te voldoen.</p>
			<p>Heeft u specifieke eisen of krijgt u liever een volledige offerte op maat?</p>
		<p>Neem dan <a href="<?php echo get_page_link(13); ?>">contact</a> met ons op en wij helpen u graag verder! Advies ter plaatse is bovendien gratis.</p>
		<p></p>
		</div>
		<div class="clearfix">
			<div class="onethird pakket-starter">
				<h3>Starter pakket</h3>
				<ul>
					<li>3 Binnencamera’s, 1 Megapixel, Infrarood tot 10M</li>
					<li>1 Buitencamera, 1 Megapixel, Infrarood tot 10M</li>
					<li>Opnametoestel voor registratie van 30 dagen</li>
					<li>Raadpleegbaar via smartphones, tablets en computers</li>
					<li>Mogelijkheid tot uitbreiding in alle aspecten</li>
					<!-- <li>Inclusief plaatsing, bekabeling en gebruiksklare afwerking*</li> -->
				</ul>
				<div class="ctas">
					<span class="btn btn-grey">Prijs op aanvraag</span>
					<a href="#" data-reveal-id="starter" class="btn">Meer info</a>
				</div>
			</div>
			<div class="onethird pakket-premium">
				<h3>Premium pakket</h3>
				<ul>
					<li>4 Binnencamera’s lens van 108°, 2 Megapixels, Infrarood tot 15M</li>
					<li>2 Buitencamera lens van 108°, 2 Megapixels, Infrarood tot 15M, vandalisme beschermd</li>
					<li>Opnametoestel voor registratie van 30 dagen</li>
					<li>Elke camera neemt geluid op</li>
					<li>1 x 24” scherm</li>
					<li>Raadpleegbaar via smartphones, tablets en computers</li>
					<li>Mogelijkheid tot uitbreiding in alle aspecten</li>
					<!-- <li>Inclusief plaatsing, bekabeling en gebruiksklare afwerking*</li> -->
				</ul>
				<div class="ctas">
					<span class="btn btn-grey">Prijs op aanvraag</span>
					<a href="#" data-reveal-id="advanced" class="btn">Meer info</a>
				</div>
			</div>
			<div class="onethird pakket-advanced">
				<h3>Enterprise pakket</h3>
				<ul>
					<li>6 Binnencamera’s lens van 108°, 2 Megapixels, Infrarood tot 15M</li>
					<li>3 Buitencamera lens van 108°, 2 Megapixels, Infrarood tot 15M, vandalisme beschermd</li>
					<li>Opnametoestel voor registratie van 30 dagen</li>
					<li>Elke camera neemt geluid op</li>
					<li>1 x 24” scherm <span class="gold-color"><strong>+ Samsung tablet</strong></span></li>
					<li>Raadpleegbaar via smartphones, tablets en computers</li>
					<li>Mogelijkheid tot uitbreiding in alle aspecten</li>
					<!-- <li>Inclusief plaatsing, bekabeling en gebruiksklare afwerking*</li> -->
				</ul>
				<div class="ctas">
					<span class="btn btn-grey">Prijs op aanvraag</span>
					<a href="#" data-reveal-id="professional" class="btn">Meer info</a>
				</div>
			</div>
		</div>

		<p style="margin-top: 20px; margin-bottom: 0;"><strong>Bovendien zijn deze pakketten inclusief plaatsing bekabeling en gebruiksklare afwerking! *</strong></p>
		<p style="font-size: 13px; color: #828282;">Bij goedkeuring van verkoper na bezoek ter plaatse.</p>

	</div>
	*/ ?>

	<div class="full-width home-references">

		<h3>Wij werken alleen met de beste producten</h3>

		<div id="home_refs" class="owl-carousel">

			<?php

			$refArgs = array(
				'post_type' => 'reference'
			);

			$refs = new WP_Query( $refArgs );

			if( $refs->have_posts() ) :
				while( $refs->have_posts() ) :
					$refs->the_post() ;

				?><div class="ref_item-wrap"><?php

					$ref_url = get_field('reference_url');

					?><a href="<?php echo $ref_url; ?>" target="_blank"><?php

					?><div class="ref_item-logo"><?php

						$ref_logo = get_field('reference_logo');
						$ref_logo_size = 'medium';
						$ref_logo_attrs = array(
							'class' => 'desaturate'
						);
						echo '<div class="ref_item_logo_wrap">' . wp_get_attachment_image( $ref_logo, $ref_logo_size, false, $ref_logo_attrs ) . '</div>';

					?></div>

					
					<?php /*
					<div class="ref_item-name">
						<?php the_title(); ?>
					</div>
					*/ ?>
					

				</a></div><?php

				endwhile;
			endif;

			?>


    	</div>
	</div>

	<?php /*
	<div class="full-width home-toolate">
		<div class="text-container">
			<p class="big">Denk niet aan camerabewking <span><strong>wanneer het te laat is...</strong></span></p>
			<a href="<?php echo get_permalink(396); ?>" class="btn-cta">Vraag nu uw gratis offerte</a>
		</div>
	</div>
	*/ ?>

<?php get_footer(); ?>