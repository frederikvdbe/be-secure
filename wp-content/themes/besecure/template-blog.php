<?php
/*
Template name: Blog
*/
?>

<?php get_header(); ?>

	<div class="content">
		<div class="onethird_column_left">

			<?php
			if( !get_field('page_content_title') ){
				?><h2 class="page-title"><?php the_title(); ?></h2><?php
			} else {	
				?><h2 class="page-title"><?php the_field('page_content_title'); ?></h2><?php
			}
			?>

			<?php

			$catTax = array( 
			    'category'
			);

			$catArgs = array(
			    'orderby'           => 'name', 
			    'order'             => 'ASC',
			    'hide_empty'        => false, 
			    'exclude'           => array(), 
			    'exclude_tree'      => array(), 
			    'include'           => array(),
			    'number'            => '', 
			    'fields'            => 'all', 
			    'slug'              => '', 
			    'parent'            => '',
			    'hierarchical'      => true, 
			    'child_of'          => 0, 
			    'get'               => '', 
			    'name__like'        => '',
			    'description__like' => '',
			    'pad_counts'        => false, 
			    'offset'            => '', 
			    'search'            => '', 
			    'cache_domain'      => 'core'
			); 

			$cats = get_terms( $catTax, $catArgs );

			if( $cats ){
				?><ul class="side list list_cats"><?php
				foreach( $cats as $cat ){
					$cat_link = get_term_link( $cat );
					echo '<li><a href="' . $cat_link . '">' . $cat->name . '</a></li>';
				}
				?></ul><?php
			}

			?>

		</div>

		<div class="twothird_column_right">

			<?php

			$blogArgs = array(
				'post_type' => 'post'
			);

			$blogs = new WP_Query( $blogArgs );

			?>

			<?php if( $blogs->have_posts() ) :
			?><ul class="blogposts"><?php
				while( $blogs->have_posts() ) :
					$blogs->the_post();
				?>

					<?php get_template_part('content', 'blog' ); ?>

				<?php
				endwhile;
				?></ul><?php
			endif;
			?>

		</div>
	</div>

<?php get_footer(); ?>