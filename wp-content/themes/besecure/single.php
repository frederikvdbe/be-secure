<?php get_header(); ?>

<div class="content">
<a style="margin-bottom:20px;" href="../nieuws/" class="btn">Terug</a>

	<?php if( have_posts() ) :
		while( have_posts() ) :
			the_post();
		?>

			<?php get_template_part('content', 'page' ); ?>

		<?php
		endwhile;
	endif;
	?>

</div>

<?php get_footer(); ?>