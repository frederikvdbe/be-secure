<li>

	<div class="product_thumb">
		<?php the_post_thumbnail( ); ?>
	</div>

	<div class="product_content">
		
		<h2 class="page-title"><?php the_title(); ?></h2>

		<?php the_content(); ?>

	</div>

</li>