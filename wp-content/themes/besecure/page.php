<?php get_header(); ?>

<?php if( has_post_thumbnail( ) ){
	echo '<div class="page-header">';
	the_post_thumbnail();
	echo '</div>';
} ?>

<?php

// This is where you run the code and display the output
$pageArgs = array(
    'sort_order' => 'ASC',
    'sort_column' => 'menu_order',
    'hierarchical' => 1,
    'exclude' => '',
    'include' => '',
    'meta_key' => '',
    'meta_value' => '',
    'authors' => '',
    'child_of' => get_the_ID(),
    'parent' => -1,
    'exclude_tree' => '',
    'number' => '',
    'offset' => 0,
    'post_type' => 'page',
    'post_status' => 'publish'
);
$children = get_pages($pageArgs);


// If page has child pages -> display sidenav
if( count($children) != 0 ){

	?>
	<div class="content">
		<div class="onethird_column_left">

			<ul class="side list childpages">
				<h3>Overzicht</h3>
			
			<?php
		    foreach( $children as $child ){
		        echo '<li class="default"><a href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a></li>';
		    }
			?>

    		</ul>

    	</div>

    	<div class="twothird_column_right">

			<?php if( have_posts() ) :
				while( have_posts() ) :
					the_post();
				?>

					<?php get_template_part('content', 'page' ); ?>

				<?php
				endwhile;
			endif;
			?>

		</div>
	</div>
    <?php

} elseif( $post->post_parent ){

    $pageArgs = array(
        'sort_order' => 'ASC',
        'sort_column' => 'menu_order',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'parent' => $post->post_parent,
        'exclude_tree' => '',
        'number' => '',
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
    );
    $children = get_pages($pageArgs);

    // If page IS child pages -> display sidenav
    if( count( $children ) != 0 ) {

    	?>

		<div class="content">
    		<div class="onethird_column_left">

    			<ul class="side list childpages"><h3>Overzicht</h3>
		
				<?php
			    foreach( $children as $child ){
			    	if( $post->ID == $child->ID ){
			    		echo '<li class="current"><a href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a></li>';
			    	} else {
			    		echo '<li class="default"><a href="' . get_permalink($child->ID) . '">' . $child->post_title . '</a></li>';
			    	}
			    }
			  	?>

			  	</ul>
			</div>

			<div class="twothird_column_right">

				<?php if( have_posts() ) :
					while( have_posts() ) :
						the_post();
					?>

						<?php get_template_part('content', 'page' ); ?>

					<?php
					endwhile;
				endif;
				?>

			</div>

		</div>

	  	<?php
	}

} else {

?>

<div class="content">

	<?php if( have_posts() ) :
		while( have_posts() ) :
			the_post();
		?>

			<?php get_template_part('content', 'page' ); ?>

		<?php
		endwhile;
	endif;
	?>

</div>

<?php

}

?>

<?php get_footer(); ?>