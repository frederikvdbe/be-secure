<?php
if( !get_field('page_content_title') ){

	?><h2 class="page-title"><?php the_title(); ?></h2><?php

} else {
	
	?><h2 class="page-title"><?php the_field('page_content_title'); ?></h2><?php

}

the_content();

?>