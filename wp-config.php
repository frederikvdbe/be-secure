<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('DB_NAME', 'besecure');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

define( 'WP_AUTO_UPDATE_CORE', false );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(J_QZ4+PH]QgS!d8U(.N8Ilt-0mE)[G|xa{Wn77;zJJ6JHATy%hkOu%m2NYvTJ|x');
define('SECURE_AUTH_KEY',  '8GNta-!K099`gN=NDfGhH!zLgqd<>.J?urd@Sq23^bT5+x|XNkOpl@^&,<_D+,YF');
define('LOGGED_IN_KEY',    '*&1U2S=|h|CY_y`U!u_AL>dlDzf@j*xU|W`-XmP-Br{rG~,I13?M9mDv|D{aP8/$');
define('NONCE_KEY',        '&*>O6jpqo+=9+%*[,}2tgmN(W!duZe+;~_X=JMGq_D^#b(4omd{U69G@ez<+HlLA');
define('AUTH_SALT',        'd/f?x0Eqoq@,mo)ra74hdtFhuz/OPCS5|ml~]wvhB!,<83$8]X^|#V[QoIqzrh@g');
define('SECURE_AUTH_SALT', 'I+)s,MW~6oz`PPa-zcXX&C{B:qv.```:!bO**+&br&yij~d(U]jS+1r@mH&w$Uf.');
define('LOGGED_IN_SALT',   'X Di@/<S8.wfQDF*L)^urIGJ9|7-NU2fi-z(_V6=n7q:N9 v/{A3./DR/U1!y=FY');
define('NONCE_SALT',       '!4|~pgjhV:%-5q*AGCt9&T~t+v6&OB{0gk4r )HuVt,9 9lc>|L(Wycs+Kpb?-;I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
